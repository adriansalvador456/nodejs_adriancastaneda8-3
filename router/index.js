import express from "express";
import json from "body-parser";
import conexion from "../models/conexion.js";
import alumnosDb from "../models/alumnos.js";

// Crear instancia de express
export const router = express.Router();

// Configurar primer ruta
router.get("/", (req, res) => {
    res.render("index", { titulo: "Practicas de nodeJS" });
});
  
router.get("/tabla", (req, res) => {
    const params = {
        numero: req.query.numero,
    };
    res.render("tabla", params);
});
  
router.post("/tabla", (req, res) => {
    const params = {
        numero: req.body.numero,
    };
    res.render("tabla", params);
});

// Practica cotizacion
router.get("/cotizacion", (req, res) => {
    const params = {
        valor: req.query.valor,
        pInicial: req.query.pInicial,
        plazos: req.query.plazos,
    };
    res.render("cotizacion", params);
});

router.post("/cotizacion", (req, res) => {
    const params = {
        valor: req.body.valor,
        pInicial: req.body.pInicial,
        plazos: req.body.plazos,
    };
    res.render("cotizacion", params);
});

let rows;
router.get("/alumnos",async (req, res) => {

    rows = await alumnosDb.mostrarTodos();

    res.render('alumnos',{reg:rows});
    
    })
    
    let params;
    router.post('/alumnos', async(req,res)=>{
        try {

        
            params ={
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domicilio:req.body.domicilio,
            sexo : req.body.sexo,
            especialidad:req.body.especialidad
            }
            const registros = await alumnosDb.insertar(params);
            console.log("-------------- registros " + registros);

            } catch(error){
                console.error(error)
                res.status(400).send("sucedio un error: " + error);
            }
            rows = await alumnosDb.mostrarTodos();
              
                res.render('alumnos',{reg:rows});
        });

        async function prueba(){
        try{
        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es", res); 
            }catch(error){
                console.log("Surgio un error", error);
            }
            finally{

            }
                 }

                 router.post("/buscar", async(req, res)=>{
                    matricula = req.body.matricula; 
                    nrow = await alumnosDb.buscarMatricula(matricula);
                    res.render("alumnos", {alu:nrow});

                    
                 })


export default { router };
